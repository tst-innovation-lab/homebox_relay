var APIBuilder = require('@axway/api-builder-runtime');
var Model = APIBuilder.createModel('alert', {
    "connector": "memory",
    "fields": {
        "category": {
            "type": "string",
            "description": "motion, smoke, water"
        },
        "value": {
            "type": "string",
            "description": "alert associated value"
        }
    },
    "actions": [
        "create",
        "read",
        "delete",
        "deleteAll"
    ]
});
module.exports = Model;